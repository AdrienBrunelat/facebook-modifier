/*
 * This file is part of Facebook Modifier <webSite>,
 *
 * Facebook Modifier is created by Acemond
 */
var cooldown = 80;
var reloading = false

//Debug vars
displayed = false

//DOM Observer
var target = document.querySelector('#facebook');
var observer = new MutationObserver(reorganize);
var config = { attributes: false, subtree: true, childList: true, characterData: false }
observer.observe(target, config);

$('#appsNav').hide();
$('#interestsNav').hide();
$('#pagesNav').hide();
$('#pagelet_ticker').hide();
$('.fbSidebarGripper').hide();

function reorganize()
{
	if (reloading) return;

	//window.alert("DOM Changed");
	reloading = true
	setTimeout("reloading = false;", cooldown);

	resetValues();
	resizeChatWindows();

	//Messages Window
	if ($('._5uj5').length)
	{
		setMessagesLayoutCSS();
		refreshMessagesLayoutCSS();
	}
	//Events, saved links, etc.
	else if ($('._5uwm').length)
	{
		$('div[role*="main"]').css('width', '100%');
		$('div[role*="complementary"]').hide();
	}
	//Must be BEFORE .home test
	//Group Page
	else if ($('.groupsCoverPhoto.fbTimelineSection').length)
	{
		
		$('#leftCol').css('position', '');
		$('#leftCol').css('height', '');
		$('#leftCol').css('border-right', '');
		$('#leftCol').css('border-right-color', '');
		//
		//TODO remove right col suggestions
	}
	//Profile Page
	else if ($('.fbTimelineSection.mtm.fbTimelineTopSection').length)
	{
		//TODO remove right col suggestions
		//$('._4-u3._2pi9').hide();
	}
	//Facebook Home Page
	else if ($('.home').length)
	{
		setHomeLayoutCSS();
		refreshHomeLayoutCSS();
	}
}

function resetValues()
{
	$('#rightCol').show()

	$('._uaw.clearfix._uaw.__wu').css('padding-right', '');

	$('#contentCol').css('padding-left', '');
	$('#contentCol').css('margin-left', '');

	var contentArea = $('#contentArea');
	contentArea.css('margin', '');
	contentArea.css('position', '');
	contentArea.css('left', '');
	contentArea.css('right', '');
	contentArea.css('width', '');
	contentArea.css('min-width', '');

	var globalContainer = $('#globalContainer');
	globalContainer.css('width', '');
	globalContainer.css('margin-left', '');
	globalContainer.css('margin-right', '');
	globalContainer.css('padding-right', '');
}

//Resizes chat windows
function resizeChatWindows()
{
	$('div[class*="fbNubFlyout fbDockChatTabFlyout uiContextualLayerParent"]').css('height', '499px');
	$('div[class*="fbNubFlyoutBody scrollable"]').css('height', '400px');
}

function setMessagesLayoutCSS()
{
	$('div[role*="complementary"]').hide();

	$('#globalContainer').css('width', 'auto');
	$('#globalContainer').css('min-width', '738px');
	$('#globalContainer').css('margin-left', '0px');
	$('#globalContainer').css('margin-right', '206px');
	$('#globalContainer').css('padding', '0px');

	$('#contentArea').css('width', '100%');

	$('div[class*="uiScrollableArea _2nc"]').css('width', 'auto');
	$('div[class*="uiScrollableAreaBody"]').css('width', 'auto');
}

function refreshMessagesLayoutCSS()
{
	var textWidth = document.getElementById('contentArea').offsetWidth - document.getElementsByClassName('wmMasterView')[0].offsetWidth;
	$('div[class*="_2nb"]').css('width', textWidth + 'px');
	$('div[class*="_2nb"]').css('min-width', '461px');
	$('div[class*="_2nb"]').css('position', 'absolute');

	var leftOffset = document.getElementsByClassName('wmMasterView')[0].offsetWidth;
	$('div[class*="_2nb"]').css('left', leftOffset + 'px');
}

function refreshHomeLayoutCSS()
{
	$('div[class*="_4-u2 mbm _5jmm _5pat _5v3q"]').css('display', 'inline-block');
	$('div[class*="_4-u2 mbm _5jmm _5pat _5v3q"]').css('text-align', 'left');
	$('div[class*="_4-u2 mbm _5jmm _5pat _5v3q"]').css('margin-left', '5px');
	$('div[class*="_4-u2 mbm _5jmm _5pat _5v3q"]').css('margin-right', '5px');
	$('div[class*="_4-u2 mbm _5jmm _5pat _5v3q"]').css('width', '470px');
	$('._3lkn').parent().css('display', 'inline-block');
	$('._3lkn').parent().css('width', '100%');
	$('._3lkn').parent().css('text-align', 'center');
	$('._3lkn').css('display', 'inline-block');
	$('._3lkn').css('width', '496px');
	$('._3lkn').css('text-align', 'center');
}

function setHomeLayoutCSS()
{
	//checkForRightCol();
	$('#rightCol').hide()

	$('#leftCol').css('position', 'fixed');
	$('#leftCol').css('height', '100%');
	$('#leftCol').css('border-right', '1px solid rgba(0, 0, 0, .4)');
	$('#leftCol').css('border-right-color', '#ccc');


	$('._uaw.clearfix._uaw.__wu').css('padding-right', '0px');
	
	$('#globalContainer').css('margin-left', '0px');

	$('#leftCol').css('padding-left', '13px');
	//$('#leftCol').css('position', 'fixed');
	
	$('#contentCol').css('padding-left', '0px');
	$('#contentCol').css('margin-left', '177px');
	
	var contentArea = $('#contentArea');
	//contentArea.css('padding', '0px 13px 0px 206px');
	contentArea.css('margin', '0px 13px 0px 206px');
	contentArea.css('position', 'absolute');
	contentArea.css('left', '0px');
	contentArea.css('right', '0px');
	contentArea.css('width', 'auto');
	contentArea.css('min-width', '496px');
	
	var globalContainer = $('#globalContainer');
	globalContainer.css('width', 'auto');
	globalContainer.css('margin-right', '206px');
	globalContainer.css('padding-right', '0px');
	
	$('.mtm').css('margin', 'auto');
	
	var pagelet_composer = $('#pagelet_composer');
	pagelet_composer.css('width', 'auto');
	pagelet_composer.css('max-width', '1002px');
	pagelet_composer.css('margin-left', '5px');
	pagelet_composer.css('margin-right', '5px');
	pagelet_composer.css('text-align', 'left');
	//$('#pagelet_composer').css('position', 'fixed');

	$('#contentCol').css('padding-top', '0px');
	$('._4-u2.mbm').css('margin-top', '12px');
	//pagelet_composer.css('width', '75%');
	//pagelet_composer.css('border-bottom', '1px solid rgba(0, 0, 0, .4)');
	//pagelet_composer.css('border-bottom-color', '#ccc');
	//pagelet_composer.css('background-color', '#e9eaed');
	//pagelet_composer.css('position', 'fixed');
	//pagelet_composer.css('z-index', '1');
	//pagelet_composer.css('left', '178px');
	//pagelet_composer.css('padding-left', '5px');
	//pagelet_composer.css('padding-right', '7px');
	////var sideBarWidth = document.getElementsByName('fbChatSidebar')[0].offsetWidth;
	//var sideBarWidth = $('.fbChatSidebar').width();
	//if ($('.fbChatSidebar').length)
	//	pagelet_composer.css('right', (sideBarWidth + 1) + 'px');
	//else pagelet_composer.css('right', '0px');
	//pagelet_composer.css('text-align', 'center');

	//$('._4-u2.mbm').css('max-width', '1002px');
	//$('._4-u2.mbm').css('text-align', 'left');
	//$('._4-u2.mbm').css('display', 'inline-block');
	//$('._4-u2.mbm').css('width', '100%');

	//var pageletHeight = document.getElementById('pagelet_composer').offsetHeight;
	//$('._5pcb').css('top', pageletHeight + 'px');
	//$('._5pcb').css('position', 'absolute');
	//$('._5pcb').css('left', '178px');
	//$('._5pcb').css('right', '206px');
	
	$('.js_2i').css('width', '496px');
	
	//Stream
	var streamPagelet = $('#stream_pagelet');
	streamPagelet.css('width', '100%');
	streamPagelet.css('min-width', '506px');
	streamPagelet.css('max-width', '1012px');
	streamPagelet.css('margin-left', 'auto');
	streamPagelet.css('margin-right', 'auto');
	streamPagelet.css('text-align', 'center');

	$('._4ikz').css('width', '100%');
	$('._4ikz').css('min-width', '496px');
}

